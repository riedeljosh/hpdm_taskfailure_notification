﻿# . $PSScriptRoot"/../Private/<private_file_name>.ps1"
# . $PSScriptRoot"/<public_file_name>.ps1"

function Program {
  [CmdletBinding()]
  param (
    [Parameter(Mandatory = $true)]
    $publicConfig
    ,
    [Parameter(Mandatory = $true)]
    $logFile
  )

  $exPol = Get-ExecutionPolicy -Scope CurrentUser
  if (-NOT("$exPol" -eq "Unrestricted")) { Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser }

  if (!(Get-Module -ListAvailable -Name PSSlack)) {
      Install-Module PSSlack -Scope CurrentUser -Force
  }
  Import-Module PSSlack

  $server = $publicConfig.server
  $database = $publicConfig.database
  $bodytemplate = $publicConfig.bodytemplate

  $qstring = "
    SELECT task_id, error_code, update_date, comment
    FROM dbo.dm_tasklog WITH (NOLOCK)
    WHERE comment LIKE '%failed%'
    AND update_date > DATEADD(hh, -24, GETDATE())
    ORDER BY update_date DESC
  "
  $results = Invoke-Sqlcmd -ServerInstance $server -Database $database -Query $qstring
  foreach($result in $results){
      $id = $result[0]
      $ec = $result[1]
      $time = $result[2]
      $comment = $result[3]
      $body = $bodytemplate.Replace("ID","$id").Replace("CODE","$ec").Replace("COM","$comment").Replace("TIME","$time")
      Send-SlackMessage -Uri $tsgwebhook -Text $body
}
}
