﻿. $PSScriptRoot"/hpdm_taskfailure_notification.ps1"
$publicConfig = Get-Content -Path $PSScriptRoot"\..\Public\config.json" -Raw | ConvertFrom-Json
$logFile = "$PSScriptRoot/../$($publicConfig.logFileName)"

function Invoke-HPDM_TaskFailure_Notification{
  param()
  try {
    # Event log checks and initialization
    # Ref: http://msdn.microsoft.com/en-us/library/system.diagnostics.eventlog.exists(v=vs.110).aspx
    # Check if Log exists
    $doesAppExist = [System.Diagnostics.EventLog]::Exists($publicConfig.logName);
    # Ref: http://msdn.microsoft.com/en-us/library/system.diagnostics.eventlog.sourceexists(v=vs.110).aspx
    # Check if Source exists
    $doesSourceExist = [System.Diagnostics.EventLog]::SourceExists($publicConfig.logSource);
    if ( !$doesAppExist -or !$doesSourceExist ) {
      New-EventLog -LogName $publicConfig.logName -Source $publicConfig.logSource
    }
    $msg = "Starting process. $(Get-Date)"
    $msg >> $logFile
    Write-EventLog -LogName $publicConfig.logName -Source $publicConfig.logSource -EventId $publicConfig.logEventId -EntryType Information -Message $msg
    Program -ErrorAction Stop -publicConfig $publicConfig -logFile $logFile
    }

    catch {
        $msg = "Top level issue:`n"
        $msg += $_.Exception.Message
        $msg += $_.Exception.StackTrace
        # $msg += $_.Exception.Source
        $msg >> $logFile
        Write-EventLog -LogName $publicConfig.logName -Source $publicConfig.logSource -EventId $publicConfig.logEventId -EntryType Error -Message $msg
        throw $_
    }

    finally {
      $msg = "Finished process. $(Get-Date)"
      $msg >> $logFile
      Write-EventLog -LogName $publicConfig.logName -Source $publicConfig.logSource -EventId $publicConfig.logEventId -EntryType Information -Message $msg
    }
}
Invoke-HPDM_TaskFailure_Notification
